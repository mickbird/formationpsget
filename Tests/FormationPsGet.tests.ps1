﻿$Verbose = @{}

if($env:APPVEYOR_REPO_BRANCH -and $env:APPVEYOR_REPO_BRANCH -notlike 'master')
{
    $Verbose.add('Verbose', $true)
}

$PSVersion = $PSVersionTable.PSVersion.Major
Import-Module $PSScriptRoot\..\FormationPsGet -Force

Describe 'Get-SomeObject' {

	Context 'Function Exists' {

		It 'Should Return ''Hi everyone'''{
			Get-SomeObject | Should Be 'Hello world !';
		}

	}
}