<#
	Get-SomeObject
#>
function Get-SomeObject {
	[CmdletBinding(SupportsShouldProcess = $true)]
	[OutputType([string])]
	Param(
		[Parameter]
		[string] $Name
	)

	begin {

	}

	process {

	}

	end {
		return 'Hello world !'
	}
}